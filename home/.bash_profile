echo "$(tput setaf 1)ETH :`/sbin/ifconfig eth0 | /bin/grep "inet addr" | /usr/bin/cut -d ":" -f 2 | /usr/bin/cut -d " " -f 1`
WIFI:`/sbin/ifconfig wlan0 | /bin/grep "inet addr" | /usr/bin/cut -d ":" -f 2 | /usr/bin/cut -d " " -f 1`$(tput sgr0)
$(tty)"

if [[ $(tty) =~ "tty" ]]
then
  screen
else
  screen -rxU
fi
