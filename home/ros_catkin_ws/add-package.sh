rosinstall_generator "$@" --rosdistro kinetic --deps --wet-only --tar > kinetic_update.rosinstall
wstool merge -t src kinetic_update.rosinstall
wstool update -t src
rosdep install --from-paths src --ignore-src --rosdistro kinetic -y -r --os=debian:jessie
sudo ./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/kinetic --pkg "$@"
