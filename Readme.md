# Wheely Project repo
This repository contains our work on the self-balancing robot /Wheely/.
The repository will host all non-catkin code and hopefully comprehensive
documentation of the code and hardware modules.

## TFT overlay
Modern raspbian uses device tree overlays for hardware configurations,
driver modules have to be configured in an overlay file in /boot/overlays 
and selected in the /boot/config.txt

The files in overlay **were** created based on the documentation found

* [here](https://www.raspberrypi.org/forums/viewtopic.php?f=91&t=111817) (post by phoenix127 (Sep 22, 2016)
* and [here](https://nonhazardo.us/raspberry/pi/tft/spi/device/tree/2016/05/30/raspberry_pi_zero_spi_screen.html)

you need to copy the .dtbo and(?) -overlay.dtb file to /boot/overlays and add
the following lines to /boot/config.txt:
```bash
dtparam=spi=on
dtoverlay=tft22
```

and to `/boot/cmdline.txt` add:
```bash
fbcon=map:10 fbcon=font:VGA8x8 logo.nologo
```

__Future improvements__ could include parametrizing the pins :-)

## Using GNU screen
In order to get a neat and clean output on the TFT we are using screen.
(it has a statusbar :-) and keeps our sessions alive on network errors :3 )
after installing screen using `sudo apt install screen` you can add the following lines to
`.bash_profile`: 
```bash
if [[ $(tty) =~ "tty" ]]
then
  screen
else
  screen -rxU
fi
```

This checks, runs a new screen instance if the current terminal is a physical terminal (tty)
otherwise it connects to the existing one (tentatively only tty1 on the tft)

To make space on the tft remove the username and path from the start of the command line.
In `.bashrc` adapt all lines with `PS1=` to only show a dollar sign.

To show the current directory in the statusbar instead add the following to the 
end of `.bashrc`: 
```bash
case $TERM in
screen*)
  PROMPT_COMMAND=set_screen_path
  ;;
esac 
```

The statusbar is configured in `.screenrc` using *backtick* and the `status.sh` script.

## Raspicam

install raspicam from [sourceforge](https://sourceforge.net/projects/raspicam/)
it seems more stable than the git version.

## ROS

Follow the [official instructions](http://wiki.ros.org/ROSberryPi/Installing%20ROS%20Kinetic%20on%20the%20Raspberry%20Pi)
to install ROS kinetic.

The `add-package.sh` script in `home/ros_catkin_ws` facilitates installing released packages,
just enter the package name(s) as attribute.

Package names are usually with underscores instead of hyphen.

## wicd
wicd seems to have some problems with `dhcpd` and IPv6, it should be configured to use `dhclient` instead to avoid these issues.
